syntax on
colorscheme badwolf

set nocompatible
set history=1000
set wrap
set mouse=a
set autoread
set spelllang=en_us
set spell
set listchars=tab:\ \ \|,eol:¬
set list

set number
set relativenumber
set ruler
set laststatus=2

set cursorline
set cursorcolumn
set colorcolumn=80
set shiftwidth=3
set tabstop=3

set nobackup
set scrolloff=10

set incsearch
set ignorecase
set smartcase
set showmatch
set hlsearch

set autoindent
set cindent

set showcmd
set wildmenu

set foldmethod=indent
set foldnestmax=4
set nofoldenable

set lazyredraw

inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap " ""<left>
inoremap ' ''<left>
inoremap jj <ESC>

inoremap <C-j> <down>
inoremap <C-k> <up>
inoremap <C-h> <left>
inoremap <C-l> <right>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

call plug#begin()

Plug 'vim-syntastic/syntastic'

call plug#end()

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

